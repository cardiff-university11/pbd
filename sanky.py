import sqlite3
from collections import defaultdict
from collections import Counter
from itertools import cycle

import plotly.graph_objects as go

DATA_DB = './source/data.db'

SELECT_OPTIONS = frozenset([
    'No relation',
    '1 is part of 2 (help to achieve)',
    '1 composed of 2 (can be achieve by)',
])

PART_RELATION = '1 is part of 2 (help to achieve)'
COMPOSED_RELATION = '1 composed of 2 (can be achieve by)'


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def con_db():
    con = sqlite3.connect(DATA_DB, check_same_thread=False)
    con.row_factory = dict_factory
    return con


def get_all_pbdes():
    con = con_db()
    cu = con.cursor()
    cu.execute('select * from pbde')
    all_pbdes = cu.fetchall()
    con.close()
    return all_pbdes


def get_all_user_select():
    con = con_db()
    cu = con.cursor()
    cu.execute('select * from user_select')
    user_selects = cu.fetchall()
    con.close()
    return user_selects


def get_link_source_to_target():
    user_selects = get_all_user_select()
    link_map = defaultdict(list)
    for s in user_selects:
        link_map[(s['x'], s['y'])].append(s)

    link_target = []

    for key, selects in link_map.items():
        selection_counter = Counter([s['selection'] for s in selects])
        top_selection, count = selection_counter.most_common(1)[0]
        if top_selection == COMPOSED_RELATION:
            link_target.append(key)
    return link_target


def show_all_data():
    all_pbdes = get_all_pbdes()
    pbde_map = {
        x['id']: x
        for x in all_pbdes
    }

    source_target = get_link_source_to_target()

    pbde_ids = []
    for x, y in source_target:
        pbde_ids.append(x)
        pbde_ids.append(y)
    pbde_ids = set(pbde_ids)
    pbdes = [pbde_map[pbde_id] for pbde_id in pbde_ids]
    principle, strategy, guideline, pattern = [], [], [], []
    for x in pbdes:
        x_title = x['title']
        x_type = x['type']
        if x_type == 'principle':
            principle.append(x_title)
        elif x_type == 'strategy':
            strategy.append(x_title)
        elif x_type == 'guideline':
            guideline.append(x_title)
        elif x_type == 'pattern':
            pattern.append(x_title)

    label = principle + strategy + guideline + pattern

    label_color_principle = ['rgba(31, 119, 180, 0.8)' for _ in range(len(principle))]
    label_color_stategy = ['rgba(255, 127, 14, 0.8)' for _ in range(len(strategy))]
    label_color_guideline = ['rgba(44, 160, 44, 0.8)' for _ in range(len(guideline))]
    label_color_pattern = ['rgba(60, 10, 255, 0.4)' for _ in range(len(pattern))]
    label_color = label_color_principle + label_color_stategy + label_color_guideline + label_color_pattern

    source_list, target_list, color_list = [], [], []
    colors = cycle([
        'rgba(214, 39, 40, 0.4)',
        'rgba(255, 127, 14, 0.4)',
        'rgba(127, 127, 127, 0.4)',
        'rgba(44, 160, 44, 0.4)',
        'rgba(188, 189, 34, 0.4)',
        'rgba(10, 189, 50, 0.4)',
        'rgba(200, 189, 34, 0.4)',
        'rgba(20, 189, 34, 0.4)',
        'rgba(60, 10, 255, 0.4)',
        'rgba(30, 10, 34, 0.4)',
        'rgba(20, 50, 100, 0.4)',
        'rgba(10, 10, 10, 0.4)',
        'rgba(255, 10, 10, 0.4)',
        'rgba(90, 10, 90, 0.4)',
        'rgba(255, 0, 0, 0.4)',
        'rgba(0, 255, 0, 0.4)',
        'rgba(10, 150, 150, 0.4)',
        'rgba(190, 10, 190, 0.4)',
        'rgba(80, 10, 255, 0.4)',
        'rgba(10, 45, 170, 0.4)',
        'rgba(255, 255, 255, 1)',
        'rgba(70, 20, 255, 1)',
        'rgba(20, 20, 85, 1)',
    ])

    x_color = {
        x: next(colors)
        for x, _ in source_target
    }

    print('data:', label, label_color)
    print('source_target', source_target)

    for x, y in source_target:
        source_list.append(label.index(pbde_map[x]['title']))
        target_list.append(label.index(pbde_map[y]['title']))
        color_list.append(x_color[x])

    print('data:', source_list, target_list, color_list)

    x1 = [0.1, 0.1]
    y1 = [0.2, 0.8]
    fig = go.Figure(
        data=[go.Sankey(
            valueformat=".0f",
            valuesuffix="TWh",

            node=dict(
                label=label,
                color=label_color,
                x=x1,
                y=y1),

            link=dict(
                source=source_list,
                target=target_list,
                value=[1 for _ in range(len(target_list))],
                label=label,
                color=color_list)
        )]
    )
    return fig


def main():
    fig = show_all_data()
    fig.update_layout(title_text="pbde", font_size=10)
    fig.show()


if __name__ == '__main__':
    main()
