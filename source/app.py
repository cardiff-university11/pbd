# coding: utf-8
import random
import sqlite3
from contextlib import contextmanager

from flask import Flask, render_template, jsonify, session, request

app = Flask(__name__)
app.secret_key = '805cf0ae79134e759b3629d1a812454f'  # session key， flask 验证时的一个加密串

SELECT_OPTIONS = frozenset([
    'No relation',
    '1 is part of 2 (help to achieve)',
    '1 composed of 2 (can be achieve by)',
])

NO_RELATION = 'No relation'
PART_RELATION = '1 is part of 2 (help to achieve)'
COMPOSED_RELATION = '1 composed of 2 (can be achieve by)'

CORRECT_PAIRS = {
    (29, 46): COMPOSED_RELATION,
    (29, 53): COMPOSED_RELATION,
    (29, 62): COMPOSED_RELATION,
    (29, 86): COMPOSED_RELATION,

    (46, 62): COMPOSED_RELATION,
    (46, 86): COMPOSED_RELATION,
    (46, 53): COMPOSED_RELATION,
    (62, 86): NO_RELATION,

    (53, 62): COMPOSED_RELATION,
    (53, 86): COMPOSED_RELATION,

    (86, 97): COMPOSED_RELATION,
    (86, 99): COMPOSED_RELATION,
    (86, 103): COMPOSED_RELATION,
    (86, 104): COMPOSED_RELATION,
    (86, 107): COMPOSED_RELATION,
    (86, 108): COMPOSED_RELATION,
    (86, 109): COMPOSED_RELATION,
    (86, 110): COMPOSED_RELATION,
    (86, 111): COMPOSED_RELATION,
    (86, 113): COMPOSED_RELATION,
    (86, 114): COMPOSED_RELATION,
    (86, 122): COMPOSED_RELATION,
    (86, 125): COMPOSED_RELATION,
    (86, 128): COMPOSED_RELATION,
    (86, 129): COMPOSED_RELATION,
    (86, 130): COMPOSED_RELATION,
    (86, 131): COMPOSED_RELATION,
    (86, 134): COMPOSED_RELATION,
    (86, 135): COMPOSED_RELATION,
    (86, 136): COMPOSED_RELATION,

    (86, 137): COMPOSED_RELATION,
    (86, 139): COMPOSED_RELATION,
    (86, 143): COMPOSED_RELATION,
    (86, 144): COMPOSED_RELATION,
    (86, 145): COMPOSED_RELATION,
    (86, 147): COMPOSED_RELATION,
    (86, 148): COMPOSED_RELATION,
    (86, 151): COMPOSED_RELATION,
    (86, 152): COMPOSED_RELATION,
    (86, 153): COMPOSED_RELATION,
    (86, 154): COMPOSED_RELATION,
    (86, 155): COMPOSED_RELATION,
    (86, 158): COMPOSED_RELATION,
    (86, 161): COMPOSED_RELATION,
    (86, 163): COMPOSED_RELATION,
    (86, 164): COMPOSED_RELATION,

    (62, 97): COMPOSED_RELATION,
    (62, 99): COMPOSED_RELATION,
    (62, 103): COMPOSED_RELATION,
    (62, 104): COMPOSED_RELATION,
    (62, 107): COMPOSED_RELATION,
    (62, 108): COMPOSED_RELATION,
    (62, 109): COMPOSED_RELATION,
    (62, 110): COMPOSED_RELATION,
    (62, 111): COMPOSED_RELATION,
    (62, 113): COMPOSED_RELATION,
    (62, 114): COMPOSED_RELATION,
    (62, 122): COMPOSED_RELATION,
    (62, 125): COMPOSED_RELATION,
    (62, 128): COMPOSED_RELATION,
    (62, 129): COMPOSED_RELATION,
    (62, 130): COMPOSED_RELATION,
    (62, 131): COMPOSED_RELATION,
    (62, 134): COMPOSED_RELATION,
    (62, 135): COMPOSED_RELATION,
    (62, 136): COMPOSED_RELATION,

    (62, 137): COMPOSED_RELATION,
    (62, 139): COMPOSED_RELATION,
    (62, 143): COMPOSED_RELATION,
    (62, 144): COMPOSED_RELATION,
    (62, 145): COMPOSED_RELATION,
    (62, 147): COMPOSED_RELATION,
    (62, 148): COMPOSED_RELATION,
    (62, 151): COMPOSED_RELATION,
    (62, 152): COMPOSED_RELATION,
    (62, 153): COMPOSED_RELATION,
    (62, 154): COMPOSED_RELATION,
    (62, 155): COMPOSED_RELATION,
    (62, 158): COMPOSED_RELATION,
    (62, 161): COMPOSED_RELATION,
    (62, 163): COMPOSED_RELATION,
    (62, 164): COMPOSED_RELATION,

    (31, 48): COMPOSED_RELATION,
    (31, 63): COMPOSED_RELATION,
    (31, 87): COMPOSED_RELATION,
    (31, 54): COMPOSED_RELATION,

    (48, 54): COMPOSED_RELATION,
    (48, 63): COMPOSED_RELATION,
    (48, 87): COMPOSED_RELATION,

    (54, 63): COMPOSED_RELATION,
    (54, 87): COMPOSED_RELATION,

    (63, 87): NO_RELATION,
    (63, 100): COMPOSED_RELATION,
    (63, 103): COMPOSED_RELATION,
    (63, 104): COMPOSED_RELATION,
    (63, 106): COMPOSED_RELATION,
    (63, 107): COMPOSED_RELATION,
    (63, 108): COMPOSED_RELATION,
    (63, 111): COMPOSED_RELATION,
    (63, 113): COMPOSED_RELATION,
    (63, 121): COMPOSED_RELATION,
    (63, 125): COMPOSED_RELATION,
    (63, 126): COMPOSED_RELATION,
    (63, 127): COMPOSED_RELATION,
    (63, 129): COMPOSED_RELATION,
    (63, 130): COMPOSED_RELATION,
    (63, 131): COMPOSED_RELATION,
    (63, 133): COMPOSED_RELATION,
    (63, 134): COMPOSED_RELATION,

    (63, 139): COMPOSED_RELATION,
    (63, 140): COMPOSED_RELATION,
    (63, 141): COMPOSED_RELATION,
    (63, 145): COMPOSED_RELATION,
    (63, 148): COMPOSED_RELATION,
    (63, 150): COMPOSED_RELATION,
    (63, 152): COMPOSED_RELATION,
    (63, 154): COMPOSED_RELATION,
    (63, 155): COMPOSED_RELATION,
    (63, 161): COMPOSED_RELATION,
    (63, 162): COMPOSED_RELATION,
    (63, 163): COMPOSED_RELATION,
    (63, 164): COMPOSED_RELATION,

    (87, 100): COMPOSED_RELATION,
    (87, 103): COMPOSED_RELATION,
    (87, 104): COMPOSED_RELATION,
    (87, 106): COMPOSED_RELATION,
    (87, 107): COMPOSED_RELATION,
    (87, 108): COMPOSED_RELATION,
    (87, 111): COMPOSED_RELATION,
    (87, 113): COMPOSED_RELATION,
    (87, 121): COMPOSED_RELATION,
    (87, 125): COMPOSED_RELATION,
    (87, 126): COMPOSED_RELATION,
    (87, 127): COMPOSED_RELATION,
    (87, 129): COMPOSED_RELATION,
    (87, 130): COMPOSED_RELATION,
    (87, 131): COMPOSED_RELATION,
    (87, 133): COMPOSED_RELATION,
    (87, 134): COMPOSED_RELATION,

    (87, 139): COMPOSED_RELATION,
    (87, 140): COMPOSED_RELATION,
    (87, 141): COMPOSED_RELATION,
    (87, 145): COMPOSED_RELATION,
    (87, 148): COMPOSED_RELATION,
    (87, 150): COMPOSED_RELATION,
    (87, 152): COMPOSED_RELATION,
    (87, 154): COMPOSED_RELATION,
    (87, 155): COMPOSED_RELATION,
    (87, 161): COMPOSED_RELATION,
    (87, 162): COMPOSED_RELATION,
    (87, 163): COMPOSED_RELATION,
    (87, 164): COMPOSED_RELATION,
}


RANDOM_POST = 0


@contextmanager
def get_db_cli_cursor():
    con = con_db()
    try:
        yield con.cursor()
    finally:
        con.close()


def dict_factory(cursor, row):

    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def con_db():

    con = sqlite3.connect(r'data.db', check_same_thread=False)
    con.row_factory = dict_factory
    return con


def get_user_by_name(username):

    with get_db_cli_cursor() as cu:

        cu.execute('select * from user where username = ?', [username])
        user_model = cu.fetchone()
    return user_model


def create_user(username, pwd):

    con = con_db()
    cu = con.cursor()

    cu.execute('''select id from user where username = ?''', [username])
    if cu.fetchone():
        return False
    cu.execute('''insert into user(username, pwd) values (?, ?)''', [username, pwd])
    con.commit()
    return True


def get_all_users():

  with get_db_cli_cursor() as cu:
        cu.execute('select * from user')
        users = cu.fetchall()
  return users


def get_all_pbde():

    with get_db_cli_cursor() as cu:
        cu.execute('select * from pbde')
        all_pbdes = cu.fetchall()
    return all_pbdes


def get_pbde_by_id(pbde_id):
    with get_db_cli_cursor() as cu:
        cu.execute('select * from pbde where id = ?', [pbde_id])
        pbde = cu.fetchone()
    return pbde


def has_user_select(user_id, x, y):
    with get_db_cli_cursor() as cu:
        cu.execute('''select id from user_select where user_id = ? AND x = ? AND y = ?''', [user_id, x, y])
        if cu.fetchone():
            return True
    return False


def get_two_random_pbde(user_id, need_correct=False):
    if need_correct:
        keys = list(CORRECT_PAIRS.keys())
        random.shuffle(keys)
        for _ in keys:
            x, y = keys[0]
            if x > y:
                x, y = y, x
            if not has_user_select(user_id, x, y):
                return get_pbde_by_id(x), get_pbde_by_id(y)


    pbde_ids = []
    for x, y in CORRECT_PAIRS:
        pbde_ids.append(x)
        pbde_ids.append(y)
    pbde_ids = list(set(pbde_ids))
    random.shuffle(pbde_ids)

    pbde_size = len(pbde_ids)
    for i in range(pbde_size):
        for j in range(i + 1, pbde_size):
            x = pbde_ids[i]
            y = pbde_ids[j]
            if x > y:
                x, y = y, x

            if not has_user_select(user_id, x, y):
                return get_pbde_by_id(x), get_pbde_by_id(y)
    return None, None


def make_cid(x, y):
    cid = f'cid{x}#{y}'
    return cid


def decode_cid(cid):
    items = cid[3:].split('#')
    return int(items[0]), int(items[1])


def create_user_select(user_id, x, y, selection):
    assert selection in SELECT_OPTIONS
    if has_user_select(user_id, x, y):
        return

    cid = make_cid(x, y)
    con = con_db()
    cu = con.cursor()
    cu.execute('insert into user_select(user_id, cid, x, y, selection) values(?, ?, ?, ?, ?)',
               [user_id, cid, x, y, selection])
    con.commit()


def get_user_selects(user_id):
    with get_db_cli_cursor() as cu:
        cu.execute('select * from user_select where user_id = ?', [user_id])
        selects = cu.fetchall()
    return selects


def get_user_select_detail(user_id):
    selects = get_user_selects(user_id)
    for selection in selects:
        selection['pbde_x'] = get_pbde_by_id(selection['x'])
        selection['pbde_y'] = get_pbde_by_id(selection['y'])
    return selects


def create_fake_post(user_id):

    n = random.random()
    if n <= 0.7:
        pbde_x, pbde_y = get_two_random_pbde(user_id, need_correct=True)  # 获取正确的 pair
    else:
        pbde_x, pbde_y = get_two_random_pbde(user_id, need_correct=False)  # 随机 pair

    key = (pbde_x['id'], pbde_y['id'])
    if key in CORRECT_PAIRS:
        selection = CORRECT_PAIRS[key]
    else:
        selection = random.choice(list(SELECT_OPTIONS))

    if pbde_x['type'] == pbde_y['type']:
        selection = NO_RELATION

    x, y = pbde_x['id'], pbde_y['id']
    create_user_select(
        user_id=user_id,
        x=x,
        y=y,
        selection=selection
    )


def is_admin_user(username, pwd):
    return username == pwd == 'admin'


@app.route('/')
def index():

    return render_template('index.html')


@app.route('/user_login/')
def user_login():

    user = request.args.get('user')
    pwd = request.args.get('pwd')
    if is_admin_user(user, pwd):
        return jsonify({'code': 0, 'msg': 'login successful', 'isAdmin': True})
    user_model = get_user_by_name(username=user)
    if user_model:
        if user_model['username'] == user and user_model['pwd'] == pwd:
            session['username'] = user_model['username']
            return jsonify({'code': 0, 'msg': 'login successful', 'isAdmin': False})

    return jsonify({'code': 1, 'msg': 'Incorrect username or password', 'isAdmin': False})


@app.route('/user/')
def user():
    username = session.get('username')
    user_model = get_user_by_name(username)
    user_id = user_model['id']
    user_select_count = len(get_user_selects(user_id))
    pbde_x, pbde_y = get_two_random_pbde(user_id)
    cid = make_cid(pbde_x['id'], pbde_y['id'])
    data = {
        'x': pbde_x,
        'y': pbde_y,
        'cid': cid,
        'user_select_count': user_select_count,
    }
    return render_template('user.html', **data)


@app.route('/user_tj/')
def user_tj():
    username = session.get('username')
    cid = request.args.get('cid')
    sel = request.args.get('sel')

    user_model = get_user_by_name(username)
    x, y = decode_cid(cid)
    pbde_x, pbde_y = get_pbde_by_id(x), get_pbde_by_id(y)
    if not pbde_x or not pbde_y:
        return jsonify({'code': 0, 'msg': 'Pbde does not exist'})

    create_user_select(
        user_id=user_model['id'],
        x=pbde_x['id'],
        y=pbde_y['id'],
        selection=sel)
    return jsonify({'code': 0, 'msg': 'Submitted successfully'})


@app.route('/admin/')
def admin():
    users = get_all_users()
    data = []
    for user_model in users:
        info = dict(**user_model)

        info['selects'] = get_user_select_detail(user_model['id'])
        data.append(info)

    return render_template('admin.html', admin_data=data)


@app.route('/admin_add/')
def admin_add():

    return render_template('admin_add.html')


@app.route('/admin_add_user/')
def admin_add_user():

    username = request.args.get('user')
    pwd = str(random.randint(100000, 999999))

    user_model = get_user_by_name(username)
    if user_model:
        return jsonify({'code': 1, 'msg': 'User already exists'})

    success = create_user(username, pwd)
    if not success:
        return jsonify({'code': 1, 'msg': 'User already exists'})

    user_model = get_user_by_name(username)
    for i in range(RANDOM_POST):
        create_fake_post(user_model['id'])

    return jsonify({'code': 0, 'msg': 'Added successfully...'})


if __name__ == '__main__':
    app.run()
