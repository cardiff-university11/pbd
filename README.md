## Required Version of Python
- `python` >= 3.7

## Used ides 
Pycharm 

## clone project
`git clone https://gitlab.com/cardiff-university11/pbd.git`

## Install related libraries
`cd c197557`
`pip install -r requirements.txt`

## Run the code
1. `sanky.py` - draw a sanky diagram by fixed data 
 - `python sankey.py`

2. `20200721_sanky1.py` - draw a sanky diagram by sqlite database
 - `python 20200721_sanky1.py`

3. web application `app.py`  - get user data by web application
 - `cd source`
 - `python app.py`
 - access `http://localhost:5000`
 
### Dependent library version display

```
click==7.1.2
Flask==1.1.2  # for web application
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
plotly==4.12.0  # for draw sanky diagram
retrying==1.3.3
six==1.15.0
Werkzeug==1.0.1
```

#### app project structure
```
.
├── app.py  # web start the program
├── data.db   # database
├── static  # some static files, like css file, js file etc.
│   └── lib
│       ├── jquery-1.12.4.js
│       └── layui
└── templates
    ├── admin.html  # admin page
    ├── admin_add.html  # user add page
    ├── index.html  # login page
    └── user.html  # user selects a page of views
```
